# socketio-example

This is a very simple Socket.IO client and server.

## Installation

npm install

## Running

```
$ node app.js
server running: http://localhost:1234/  (Ctrl+C to Quit)
```

Web page:

![](public/browser.png)





