var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var app = express();
var server = require('http').createServer(app);  
var io = require('socket.io').listen(server);
var PORT = 1234;

// Start the server
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({type: 'application/vnd/api+json'}));
app.use(express.static(__dirname + '/public'));

// Emit welcome message on connection
io.on('connection', function(socket) {
  // Use socket to communicate with this particular client only, sending it it's own id
  socket.emit('welcome', { message: 'Welcome! Your id is ' + socket.id, id: socket.id });

  socket.on('marco',  function(data){
    io.emit('polo', { message: 'Polo!'})
  });
});

server.listen(PORT);
console.log('server running: http://localhost:%s/  (Ctrl+C to Quit)', PORT);

// Handle Ctrl-C (graceful shutdown)
process.on('SIGINT', function() {
  console.log('Exiting...');
  process.exit(0);
});